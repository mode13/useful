/* bin.c -- Convert binary input to chosen base
**
** COMPILING:
** This version is in C, remember to -lm to
** link the math library with gcc
** gcc -o3 -Wall -Werror bin.c -o bin -lm
**
** V.0.3 - Added support for spacing between nibbles
** Fixed bug limiting program to working with 63-bit values
** Added long options
**
** TODO: Since a few of the functions rely on binToDec(), supposing
** the user calls bin -dx or -xo etc, binToDec() will be called repeatedly
** rather than only once. If binToDec() gets called, set a flag and store
** the results into a variable for the other functions to check first.
**
** Allow printing the final result assuming signed input values
*/

#include <stdio.h>
#include <math.h>
#include <getopt.h>
#include <stdint.h>
#include <stdlib.h> 
#include <string.h>

// P R O T O T Y P E S 
int help(void);
int verify(char *bin);
unsigned long int binToDec(char *bin);
int binToOct(char *bin);
int binToHex(char *bin);


// D E F I N E S 
#define MAX_INPUT 16
#define NIBBLE     4

// decimal to hex lookup table
static const char hex[] =
{
    [ 0] = '0',
    [ 1] = '1',
    [ 2] = '2',
    [ 3] = '3',
    [ 4] = '4',
    [ 5] = '5',
    [ 6] = '6',
    [ 7] = '7',
    [ 8] = '8',
    [ 9] = '9',
    [10] = 'A',
    [11] = 'B',
    [12] = 'C',
    [13] = 'D',
    [14] = 'E',
    [15] = 'F'
};

//////////////// M A I N /////////////////////////////////////////////////
int main (int argc, char** argv)
{
    
    int16_t c = 0;
    int hex = 0;	// FLAGS
    int dec = 0;
    int oct = 0;
    
    char *usage =  "usage: bin -[[x][h][d][o] <binary value to convert>\n"\
                    "e.g. bin -d 10110111\n\nbin --help for details\n";
                     
    static struct option long_options[] =
    {
        { "help",	no_argument,	0,	1},
        { "hex",	no_argument,	0,	2},
        { "octal",	no_argument,	0,	3},
        { "decimal",	no_argument,	0,	4},
        { NULL,		0,		NULL,	0},
    };
    
    while ((c = getopt_long(argc, argv, "hxod", long_options, NULL)) != -1)
    {
        switch (c) 
        {
            // help
            case 'h':
            case  1:
            {
                printf ("??\n");                
                help();
                return 0;
                break;
            }
            
            // hex
            case 'x':
            case 2:
            {
                hex = 1;
                break;
            }
            
            // octal
            case 'o':
            case 3:
            {
                oct = 1;
                break;
            }
            
            // decimal
            case 'd':
            case 4:
            {
                dec = 1;
                break;
            }
            
            // garbage
            case '?':
            {
                fprintf (stderr, "unknown option\n");
                exit(1);
                break;
            }
            
            default:
            {
                fprintf (stderr, "Mandatory option missed\n");
                exit(1);
                break;
            }
            
        } // end switch
        
    } // end while
            
    // If user didn't give any options, print hex
    if (oct == 0 && dec == 0 && hex == 0)
    {
        printf ("%s\n", usage);
        hex = 1;
    }
                
    // Make sure user entered something after the options
    if (optind == argc)
    {
        printf ("You need to enter a binary value.. \n");
        return 2;
    }
    
    // storage for binary value input
    char *binVal = calloc((MAX_INPUT * NIBBLE + 1), sizeof(char) );
    int  length  = 0;
    // There is a non option argument (expecting binary value now)
    while (optind < argc)
    {
        // copy binary value into string (not error checking input yet)
        if (strlen(argv[optind]) > 0 && strlen(argv[optind]) <= MAX_INPUT)
        {
            length += strlen(argv[optind]);
            if (length <= (MAX_INPUT * NIBBLE) )	// don't allow overflow
            {
                strcat(binVal, argv[optind]);
                ++optind;
            }
            
            else
            {
                fprintf(stderr, "Overflow detected, exiting\n");
                exit(2);
            }
        }
        
        else
        {
            fprintf (stderr, "Enter a 64-bit maximum binary value\n");
            return 3;
        }    
        
    } // end if additional arguments
    
    // binVal returns 1 on failure
    if (verify(binVal))
    {
        return 5;
    }
   
    // now we have a proper binary string, do conversions
    if (dec == 1)
       printf("%lu\n", binToDec(binVal));
        
    if (oct == 1)
        binToOct(binVal);
        
    if (hex == 1)
        binToHex(binVal);
        
    free(binVal);
    
    return 0;
    
} // end main
        
        
/*************************************************************************
* binToDec()
* This function converts a string containing a binary value
* into a decimal integer. Note that the binary string must
* be concatenated. Remove all spaces first.
*************************************************************************/        
unsigned long int binToDec(char *bin)
{
    unsigned long int decimal  = 0;
    int 	      exponent = 0; 
    int length = strlen(bin);
    int i = length - 1;	// -1 because we iterate to 0 on the array

    while (i != -1) // 0 is the first character, you need it
    {
        // PITFALL: pow() returns a double, this will break at 63 bits
        // if you don't cast to unsigned long!!!
        decimal += (unsigned long)( (bin[i] - 48) * pow(2, exponent) );
        exponent++;
        i--;     
    }
    
    return decimal;
    
} //end binToDec()
        
/*************************************************************************
* binToOct()
* Convert hexadecimal value to octal, relies on 
* binToDec() to work
**************************************************************************/
int binToOct(char *bin)
{
    // For ease, convert the hex value to decimal
    uint64_t decimal  = binToDec(bin);
    char * result = calloc(21, sizeof(char));
    int i = 0;

    /* Algorthm is the same as decimal:
    ** Divide by 8, remainder becomes rightmost digit
    ** quotient becomes new value. Loop until 0 and then
    ** print result */
    while (decimal != 0)
    {
        result[i++] = ( decimal % 8 );
        decimal /= 8;
    } 
    
    printf ("0");
    // Print resulting octal value 
    for (--i; i >= 0; --i)
    {
        printf ("%d", result[i]);
    }
      
    printf ("\n");    
    return 0;
} 

/*************************************************************************
* binToHex()
* Convert binary value into hexadecimal, relies on
* binToDec() to work
*************************************************************************/
int binToHex(char *bin)
{
    unsigned long int decimal = binToDec(bin);
    int i = 0;
    char * result = calloc(16, sizeof(char));
    
    // Algorithm: Divide by 16, remainder becomes the
    // rightmost digit, quotient becomes new value, loop
    // until 0 and then return/print the result
    while (decimal != 0)
    {
        result[i++] = ( decimal % 16 );
        decimal /= 16;
    }
    
    // print result in proper order
    printf ("0x");
    for (--i; i >= 0; --i)
    {
        printf ("%c", hex[(int)result[i] ]);
    }
    
    printf ("\n");
    return 0;
} 

/*************************************************************************
* help()
* This program helps people who are in need 
* Do not abuse the privilege
*************************************************************************/   
int help (void)
{
    printf ("\n\n     Binary Conversion     		\n\n"\
            "--------------------------------		\n"\
            "   OPTIONS 			     	\n"\
            "-x   | --hex       Convert to hex    	\n"\
            "-d   | --decimal   Convert to decimal   	\n"\
            "-o   | --octal     Convert to octal	\n"\
            "\nex: bin -d 00110010			\n\n");
            
    return 0;
           
}        
        
/*************************************************************************
* verify()
* This function simply verifies that the final resulting string
* contains only 0 and 1. Note it must be concatenated for the
* other functions to work, so spaces will result in failure
**************************************************************************/    
int verify (char *bin)
{
    // verify input string consists only of '0' and '1'
    int length = strlen(bin);
    
    for (int i = 0; i < length; ++i)
    {
        if (bin[i] != '0' && bin[i] != '1')
        {
            fprintf (stderr, "Garbage value in %s\n", bin);
            return 1;
        }
    }
    
    // if it made it this far, it's binary
    return 0;
    
}     
    
    

